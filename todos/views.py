from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateNew, NewItem

# Create your views here.


# the list all function
def todo_list_all(request):
    all_todos = TodoList.objects.all()
    context = {
        "todo_list_all": all_todos,
    }
    return render(request, "todos/list.html", context)


# create a view that shows the details
# of a particular to-do list, including tasks
def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {"list": list}
    return render(request, "todos/detail.html", context)


# Create a create view for the TodoList model that will show
# the name field in the form and handle the form submission
# to create a new TodoList


def create_new(request):
    if request.method == "POST":
        form = CreateNew(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = CreateNew()

        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


# Create an update view for the TodoList model that will show
# the name. field in the form and handle the form submission
# to change an existing TodoList


def update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateNew(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = CreateNew(instance=todo_list)

    context = {
        "form": form,
        "todo_list_all": todo_list,
    }
    return render(request, "todos/edit.html", context)


# Create a delete view for the TodoList model that will
# show a # delete button and handle the form submission
# to delete an existing TodoList.


def delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# Create a create view for the TodoItem model that will
# show the task field in the form and handle the form
# submission to create a new TodoItem.


def item_create(request):
    if request.method == "POST":
        form = NewItem(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    context = {"lists": TodoList.objects.all()}
    return render(request, "todos/item_create.html", context)
