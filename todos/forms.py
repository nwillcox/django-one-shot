from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateNew(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class NewItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
        ]
