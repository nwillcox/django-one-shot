from django.urls import path
from todos.views import (
    todo_list_all,
    todo_list_detail,
    create_new,
    update,
    delete,
    item_create,
)

urlpatterns = [
    path("", todo_list_all, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("todos/create/", create_new, name="todo_list_create"),
    path("<int:id>/edit/", update, name="todo_list_update"),
    path("<int:id>/delete/", delete, name="todo_list_delete"),
    path("items/create/", item_create, name="todo_item_create"),
]
